
= build a version of LLVM & Clang frozen at a specific version

run 'bash build.sh'

== LLVM

file:            llvm-4.0.1.src.tar.xz
downloaded from: http://releases.llvm.org/4.0.1/llvm-4.0.1.src.tar.xz
on:              Wed Jul 26 2017
SHA-1:           ddbf4dab7ab1a4bdce5ed1227bf721cbb3533a6e llvm-4.0.1.src.tar.xz

== Clang

file:            cfe-4.0.1.src.tar.xz
downloaded from: http://releases.llvm.org/4.0.1/cfe-4.0.1.src.tar.xz
on:              Wed Jul 26 2017
SHA-1:           d95d1f3a67b5448f25338bf657a7d379dbf2312f  cfe-4.0.1.src.tar.xz

== lld

file:            lld-4.0.1.src.tar.xz
downloaded from: http://releases.llvm.org/4.0.1/cfe-4.0.1.src.tar.xz
on:              Wed Jul 26 2017
SHA-1:           884d9dddf9238c2fecbefbaee784aa6191e2b9d0  lld-4.0.1.src.tar.xz
