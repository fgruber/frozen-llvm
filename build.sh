#!/bin/bash

readonly PATH=/usr/bin

if /usr/bin/[ "x$BASH" = "x" ]; then
  builtin echo 'warning: This script is meant to be run wish bash' >&2
fi

builtin set -o errexit
builtin set -o nounset
builtin set -o pipefail
builtin set -x

### SETUP VARIABLES

readonly ROOT="$PWD"

readonly SOURCE_DIR="$ROOT/source"
readonly BUILD_DIR="$ROOT/build"
readonly INSTALL_DIR="$ROOT/install"
readonly LOG_DIR="$ROOT/logs"

readonly LLVM_TARBALL="$ROOT/llvm-4.0.1.src.tar.xz"
readonly CLANG_TARBALL="$ROOT/cfe-4.0.1.src.tar.xz"
readonly LLD_TARBALL="$ROOT/lld-4.0.1.src.tar.xz"

readonly LLVM_SOURCE_DIR="$SOURCE_DIR/llvm-4.0.1.src"
readonly CLANG_SOURCE_DIR="$SOURCE_DIR/cfe-4.0.1.src"
readonly LLD_SOURCE_DIR="$SOURCE_DIR/lld-4.0.1.src"

builtin cd "$ROOT"

### CLEAN BUILD ENVIRONMENT

BUILD_ARTIFACTS=(
  "$SOURCE_DIR"
  "$BUILD_DIR"
  "$INSTALL_DIR"
  "$LOG_DIR"
)
readonly BUILD_ARTIFACTS

builtin echo '! WARNING WILL DELETE ALL BUILD FOLDERS & LOG FILES:' ${BUILD_ARTIFACTS[@]} '!'
builtin read -p 'PRESS ENTER TO CONTINUE'

for ARTIFACT in "${BUILD_ARTIFACTS[@]}"
do
  builtin set +e
  /usr/bin/rm -r "$ARTIFACT"
  builtin set -e
done


### LOG ENVIRONMENT

mkdir "$LOG_DIR"

builtin echo > "$LOG_DIR/version.log"

/usr/bin/bash  --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/cmake --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/cc    --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/c++   --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/ld    --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"

/usr/bin/ninja --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"

/usr/bin/tar   --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/xz    --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/cat   --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/tee   --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/uname --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/rm    --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/mkdir --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"
/usr/bin/date  --version 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"

/usr/bin/cat /etc/fedora-release 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"

/usr/bin/uname -a 2>&1 | /usr/bin/tee -a "$LOG_DIR/version.log"

/usr/bin/date --iso-8601 2>&1 | /usr/bin/tee "$LOG_DIR/date.log"

/usr/bin/env 2>&1 | /usr/bin/tee "$LOG_DIR/env.log"

### SETUP ENVIRONMENT

/usr/bin/mkdir "$SOURCE_DIR"
/usr/bin/mkdir "$BUILD_DIR"
/usr/bin/mkdir "$INSTALL_DIR"


### UNPACK SOURCE TARBALLS

builtin pushd "$SOURCE_DIR"
/usr/bin/tar -xf "$LLVM_TARBALL"
/usr/bin/tar -xf "$CLANG_TARBALL"
/usr/bin/tar -xf "$LLD_TARBALL"
builtin popd


### RUN CMAKE

builtin pushd "$BUILD_DIR"
CMAKE_FLAGS=(
  -GNinja
  -DCMAKE_BUILD_TYPE:STRING=Release
  -DCMAKE_C_COMPILER=/usr/bin/cc
  -DCMAKE_CXX_COMPILER=/usr/bin/c++
  -DCMAKE_INSTALL_PREFIX:PATH="$INSTALL_DIR"
  -DBUILD_SHARED_LIBS:BOOL="OFF"
  -DLLVM_TARGETS_TO_BUILD="X86"
  -DLLVM_EXTERNAL_CLANG_SOURCE_DIR="$CLANG_SOURCE_DIR"
  -DLLVM_EXTERNAL_LLD_SOURCE_DIR="$LLD_SOURCE_DIR"
  "$LLVM_SOURCE_DIR"
)
readonly CMAKE_FLAGS
builtin echo /usr/bin/cmake "${CMAKE_FLAGS[@]}" > "$LOG_DIR/cmake.log"
/usr/bin/cmake "${CMAKE_FLAGS[@]}" 2>&1 | /usr/bin/tee -a "$LOG_DIR/cmake.log"
builtin popd


### BUILD

builtin pushd "$BUILD_DIR"
/usr/bin/ninja install 2>&1 | /usr/bin/tee "$LOG_DIR/ninja.log"
builtin popd

